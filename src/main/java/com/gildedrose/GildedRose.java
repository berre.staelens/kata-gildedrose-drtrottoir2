package com.gildedrose;

class GildedRose {
    Item[] items;
    int normalDegradation = 1;
    int conjuredDegradation = normalDegradation * 2;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {
            String type = getItemType(items[i]);
            items[i].sellIn -= 1;
            // To do sellIn -1 on all to later & put Sulfuras back to 0 orrr to do if below? That is the question...
            // if (type != "Sulfuras") {
            //     items[i].sellIn -= 1;
            // }
            switch (type) {
                case "Normal":
                    updateNormal(items[i]);
                    break;
                case "Aged":
                    updateAged(items[i]);
                    break;
                case "Backstage":
                    updateBackstage(items[i]);
                    break;
                case "Conjured":
                    updateConjured(items[i]);
                    break;
                case "Sulfuras":
                    items[i].quality = 80;
                    items[i].sellIn = 0;
                    break;
                default:
                    // do nothing
            }
        }
    }

    public String getItemType(Item item) {
        if (item.name.equals("Aged Brie")) {
            return "Aged";
        } else if (item.name.contains("Backstage passes")) {
            return "Backstage";
        } else if (item.name.contains("Conjured")) {
            return "Conjured";
        } else if (item.name.equals("Sulfuras, Hand of Ragnaros")) {
            return "Sulfuras";
        } else {
            return "Normal";
        }
    }

    public void updateAged(Item item) {
        if (item.quality < 50) {
            item.quality += 1;
        } else {
            item.quality = 50;
        }
    }

    public void updateBackstage(Item item) {
        if (item.sellIn < 0) {
            item.quality = 0;
        } else {
            if ((item.sellIn <= 10) && (item.quality < 50)) {
                if (item.sellIn <= 5) {
                    item.quality += 3;
                } else {
                    item.name += 2;
                }
            }
            if (item.quality > 50) {
                item.quality = 50;
            }
        }
    }

    public void updateNormal(Item item) {
        update(item, normalDegradation);
    }

    public void updateConjured(Item item) {
        update(item, conjuredDegradation);
    }

    private void update(Item item, int degradation) {
        int mod = 1;
        if (item.sellIn < 0) {
            mod = 2;
        }
        if ((item.quality -= degradation * mod) >= 0) {
            item.quality -= degradation * mod;
        } else {
            item.quality = 0;
        }
    }
}